#include <stdio.h>
#include "math.h"

int main()
{
    int num1 = 50;
    int num2 = 10;

    printf("\n");

    printf("Scitani: %d\n", Scitani(num1, num2));
    printf("Odecitani: %d\n", Odecitani(num1, num2));
    printf("Nasobeni: %d\n", Nasobeni(num1, num2));
    printf("Deleni: %d\n", Deleni(num1, num2));

    printf("\n");

    return 0;
}