#include <stdio.h>

int Scitani(int x, int y)
{
    return x + y;
}

int Odecitani(int x, int y)
{
    return x - y;
}

int Nasobeni(int x, int y)
{
    return x * y;
}

int Deleni(int x, int y)
{
    if (y != 0)
        return x / y;
    else
        return -1;
}